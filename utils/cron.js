const fs = require('fs');
const schedule = require('node-schedule');

const client = require('./discordClient');
const getGames = require('./getGames');
const embed = require('./embed');

const logger = require('./logger');

const cacheFile = `${__dirname}/../persist/cache.json`;

let cache;
let scheduled;

try {
  cache = fs.readFileSync(cacheFile, 'utf8');
} catch (err) {
  logger('Cache file not found.');
}

const noRealUpdateInGames = (games) => {
  const processedList = (fullList) => JSON.stringify(
    fullList
      .map((g) => ({ title: g.title, upcoming: g.upcoming }))
      .sort((a, b) => a.title.localeCompare(b.title)),
  );

  if (!cache) return false;

  const listA = processedList(games);
  const listB = processedList(JSON.parse(cache));

  return JSON.stringify(listA) === JSON.stringify(listB);
};

const job = async () => {
  logger('Started running.');
  const games = await getGames();

  if (cache === JSON.stringify(games)) {
    logger('No new games.');
    return;
  }

  if (noRealUpdateInGames(games)) {
    logger('Same games with no updates.');
    return;
  }

  logger('New games found!');
  logger({ cache, games });

  cache = JSON.stringify(games);
  try {
    await fs.writeFileSync(cacheFile, JSON.stringify(games), 'utf8');
  } catch (err) {
    logger('Can\'t write cache file');
    console.error(err);
  }

  const updatesChannels = [];

  try {
    client.guilds.forEach((g) => g.channels.forEach((c) => {
      if (c.name === 'updates') updatesChannels.push(c);
    }));
  } catch (err) {
    logger(err);
    throw new Error('Can\'t read guilds');
  }

  updatesChannels.forEach((uc) => {
    if (!uc.permissionsFor(uc.guild.me).has('SEND_MESSAGES') || !uc.permissionsFor(uc.guild.me).has('EMBED_LINKS')) {
      const general = uc.guild.channels.find((c) => c.name === 'general');
      general.send('I don\'t have the required permissions in your \'updates\' channel. Don\'t forget to allow me to send messages and embed links in it.');
      logger(`Missing permissions @ ${uc.guild.name} server`, 'error');
      return;
    }
    try {
      uc.send('New free EpicGames this week:');
      games.forEach((o) => { uc.send(embed(o)); });
    } catch (err) {
      logger(err);
      throw new Error('Can\'t send message');
    }
  });
};

const cron = (channel) => {
  try {
    if (scheduled) {
      if (!channel || !cache) return;
      const games = JSON.parse(cache);
      channel.send('New free EpicGames this week:');
      games.forEach((o) => { channel.send(embed(o)); });
      return;
    }
    job();
    scheduled = schedule.scheduleJob('0 */2 * * *', job); // Every two hours
    // scheduled = schedule.scheduleJob('* * * * *', job); // Every minute
  } catch (err) {
    logger(err, 'error');
  }
};

module.exports = cron;
