require('dotenv').config();

const Discord = require('discord.js');

const logger = require('./logger');

const client = new Discord.Client();

const { DISCORD_BOT_TOKEN } = process.env;

if (!DISCORD_BOT_TOKEN) {
  logger('Missing DISCORD_BOT_TOKEN');
  process.exit();
}

client.login(DISCORD_BOT_TOKEN);

module.exports = client;
