const fetch = require('node-fetch');

const logger = require('./logger');

module.exports = () => new Promise(async (resolve, reject) => { // eslint-disable-line no-async-promise-executor, max-len
  try {
    const res = await fetch('https://store-site-backend-static.ak.epicgames.com/freeGamesPromotions?locale=es-ES&country=AR&allowCountries=AR');
    const epic = await res.json();
    const { elements: games } = epic.data.Catalog.searchStore;

    const offers = games
      .filter((game) => game.promotions)
      .map((game) => ({
        title: game.title,
        slug: game.productSlug,
        image: game.keyImages.find((i) => i.type === 'DieselStoreFrontWide').url,
        date: game.promotions.promotionalOffers.length
          && game.promotions.promotionalOffers[0].promotionalOffers[0].startDate,
        upcoming: game.promotions.upcomingPromotionalOffers.length
          && game.promotions.upcomingPromotionalOffers[0].promotionalOffers[0].startDate,
      }))
      .sort((a, b) => new Date(a.upcoming) - new Date(b.upcoming));

    resolve(offers);
  } catch (err) {
    logger(err, 'error');
    reject(err);
  }
});
